package com.example.sumup11.util


import com.example.sumup11.network.SimpleApi
import com.example.sumup11.network.model.messagesItem
import com.example.sumup11.resource.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject

class Repository @Inject constructor(private val api: SimpleApi){

    suspend fun getPost(): Flow<Resource<List<messagesItem>>> {
        return flow {
            emit(handleResponse ({api.getCustomPost()}, "Something went wrong"))
        }
    }

}

private suspend fun <T> handleResponse(
    request: suspend () -> Response<T>,
    defaultMessage: String
): Resource<T> {
    return try {
       // Resource.isLoading<Boolean>(true).loadState
        val result = request.invoke()
        val body = result.body()
        if (result.isSuccessful && body != null) {
            //Resource.isLoading<Boolean>(false).loadState
            Resource.Success(body)
        } else {
            // Only for hackers -> Here you can parse error body
            Resource.Error(result.message() ?: defaultMessage)
        }
    } catch (e: Throwable) {
        Resource.Error("Something went wrong!", null)
    }
}