package com.example.sumup11

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup11.databinding.MessageModelBinding
import com.example.sumup11.extensions.setImage
import com.example.sumup11.extensions.setVisibility
import com.example.sumup11.network.model.messagesItem
import com.example.sumup11.resource.MessageType

class MessageAdapter: RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {
    private val messages = mutableListOf<messagesItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageAdapter.MessageViewHolder {
       return MessageViewHolder(MessageModelBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: MessageAdapter.MessageViewHolder, position: Int) {
        val model = messages[position]
        holder.onBind(model)
    }

    override fun getItemCount() = messages.size

    inner class MessageViewHolder(private val binding: MessageModelBinding): RecyclerView.ViewHolder(binding.root) {

        fun onBind(model: messagesItem) {

            with(binding) {
                message.text = model.lastMessage.toString()
                name.text = model.firstName.toString().plus(model.lastName.toString())
                icon.setImage(model.avatar)

                if(model.unreaMessage == 0) {
                    message.setTextColor(Color.parseColor("#96A7AF"))
                    time.setTextColor(Color.parseColor("#96A7AF"))
                    unreadMessage.setVisibility(false)
                } else {
                    unreadMessage.setVisibility(true)
                    unreadMessage.text = model.unreaMessage.toString()
                }

                when (model.messageType) {
                    MessageType.TEXT.type -> {
                        attachment.setVisibility(false)
                    }
                    MessageType.VOICE.type -> {
                        attachment.setImageResource(R.drawable.ic_shape)
                        message.setText(R.string.voice)
                    }
                    else -> {
                        message.setText(R.string.attachment)
                    }
                }

                if(model.isTyping == false) {
                    isTyping1.setVisibility(true)
                    isTyping2.setVisibility(true)
                }  else {
                    isTyping1.setVisibility(false)
                    isTyping2.setVisibility(false)
                }
            }

        }
    }

    fun setData(messagesItem: List<messagesItem>) {
        messages.clear()
        messages.addAll(messagesItem)
        notifyDataSetChanged()
    }
}