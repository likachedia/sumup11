package com.example.sumup11.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sumup11.network.model.messagesItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class MessageViewModel @Inject constructor(private val repository: Repository): ViewModel() {
    private val _response = MutableStateFlow(mutableListOf<messagesItem>())
    val response get() = _response

    fun getPost() {
        viewModelScope.launch {
            repository.getPost().onEach {
               if(!it.data.isNullOrEmpty()) {
                   _response.value = it.data.toMutableList()
               }

            }.launchIn(viewModelScope)
        }
    }
}