package com.example.sumup11.network

import okhttp3.Interceptor

import okhttp3.Response

class MessageInterceptor:Interceptor{
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("Auth-Token", "QpwL5tke4Pnpja7X4")
            .build()
        return chain.proceed(request)
    }
}