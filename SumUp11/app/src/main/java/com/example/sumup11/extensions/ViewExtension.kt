package com.example.sumup11.extensions

import android.util.Log
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.sumup11.R

fun ImageView.setImage(url: String?) {
    if (!url.isNullOrEmpty())
        Glide.with(context).load(url).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher)
            .into(this)
    else
        setImageResource(R.mipmap.ic_launcher)
}

fun View.setVisibility(boolean: Boolean) {
    if(boolean) {
        this.visibility =  View.VISIBLE
    } else {
        this.visibility =   View.GONE
    }
}