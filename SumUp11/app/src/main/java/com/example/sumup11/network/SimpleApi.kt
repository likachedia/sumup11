package com.example.sumup11.network


import com.example.sumup11.network.model.messagesItem
import retrofit2.Response
import retrofit2.http.GET

interface SimpleApi {

  @GET("v3/80d25aee-d9a6-4e9c-b1d1-80d2a7c979bf")
    suspend fun getCustomPost(
    ): Response<List<messagesItem>>
}