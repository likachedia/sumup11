package com.example.sumup11.resource

enum class MessageType(val type: String) {
    TEXT("text"),
    ATTACHMENT("attachment"),
    VOICE("voice")

}