package com.example.sumup11.network


import com.example.sumup11.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitInstance {
   // const val BASE_URL = "https://run.mocky.io/"


    val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Singleton
    @Provides
    fun providesClient() : OkHttpClient {
       return  OkHttpClient.Builder().apply {
            addInterceptor(loggingInterceptor)
        }.build()
    }

//            .client(client)
//
    @Singleton
    @Provides
    fun  providesRetrofit(moshi: Moshi): Retrofit {
       return  Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Singleton
    @Provides
    fun api(retrofit: Retrofit): SimpleApi {
       return retrofit.create(SimpleApi::class.java)
    }

    @Singleton
    @Provides
    fun moshiConvertor(): Moshi {
        return Moshi.Builder().addLast(KotlinJsonAdapterFactory())
                .build()

    }

}