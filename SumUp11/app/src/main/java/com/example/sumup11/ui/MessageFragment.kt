package com.example.sumup11.ui

import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sumup11.MessageAdapter

import com.example.sumup11.base.BaseFragment
import com.example.sumup11.databinding.FragmentMessageBinding
import com.example.sumup11.extensions.setVisibility
import com.example.sumup11.resource.Resource
import com.example.sumup11.util.MessageViewModel
import kotlinx.coroutines.flow.collect


class MessageFragment : BaseFragment<FragmentMessageBinding>(FragmentMessageBinding::inflate) {
    private val viewModel: MessageViewModel by activityViewModels()
    private lateinit var messAdapter: MessageAdapter

    override fun start() {
        binding.progress.setVisibility( true)
        viewModel.getPost()
        initRecycler()
        obsserve()
    }

      private fun initRecycler() {
        messAdapter = MessageAdapter()
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = messAdapter
        }
    }

    private fun obsserve() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.response.collect {
                    if(it.isNotEmpty()) {
                        binding.progress.setVisibility( false)
                        messAdapter.setData(it)
                    }
                }
            }
        }

    }
}